# Danish translation for gnome-multi-writer.
# Copyright (C) 2019 gnome-multi-writer's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-multi-writer package.
# scootergrisen, 2019.
# Alan Mortensen <alanmortensen.am@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: gnome-multi-writer master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-multi-writer/"
"issues\n"
"POT-Creation-Date: 2021-05-13 18:50+0000\n"
"PO-Revision-Date: 2022-03-21 17:42+0100\n"
"Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.3\n"

#. TRANSLATORS: the application name
#. set the title
#. TRANSLATORS: Application window title
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:8
#: data/org.gnome.MultiWriter.desktop.in:3 src/gmw-main.c:772
#: src/gmw-main.ui:98
msgid "MultiWriter"
msgstr "Multiskriver"

#. TRANSLATORS: one-line description for the app
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:10
#: data/org.gnome.MultiWriter.desktop.in:4 src/gmw-main.c:1246
msgid "Write an ISO file to multiple USB devices at once"
msgstr "Skriv en ISO-fil til flere USB-enheder på samme tid"

#. TRANSLATORS: AppData description marketing paragraph
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:13
msgid ""
"GNOME MultiWriter can be used to write an ISO file to multiple USB devices "
"at once. Supported drive sizes are between 1GB and 32GB."
msgstr ""
"GNOME Multiskriver kan bruges til at skrive en ISO-fil til flere USB-enheder "
"på samme tid. Drevstørrelser mellem 1 GB og 32 GB understøttes."

#. TRANSLATORS: QA refers to quality assurance, aka people testing stuff,
#. GNOME refers to the desktop environment
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:20
msgid ""
"MultiWriter may be useful for QA testing, to create a GNOME Live image for a "
"code sprint or to create hundreds of LiveUSB drives for a trade show."
msgstr ""
"Multiskriver kan være nyttig til kvalitetstests, til at oprette et GNOME "
"Live-aftryk til et kodesprint eller til at oprette hundredvis af LiveUSB-"
"drev til en handelsmesse."

#. TRANSLATORS: saturate as in the throughput can get no more
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:26
msgid ""
"Writing a more than 10 devices simultaneously can easy saturate the USB bus "
"for most storage devices. There are two ways to write more devices in "
"parallel:"
msgstr ""
"Når der skrives til flere end 10 enheder på samme tid, så kan det let fylde "
"de fleste lagerenheders USB-bus op. Der findes to måder til at skrive til "
"flere enheder parallelt:"

#. TRANSLATORS: storage devices refers to the things we're writing to
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:33
msgid "Use USB 3.0 hubs, even if the storage devices are USB 2.0"
msgstr "Brug USB 3.0-hubber, selv hvis lagerenheden er USB 2.0"

#. TRANSLATORS: PCIe is the data bus, don't translate please
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:35
msgid "Install another USB 2.0 PCIe root hub"
msgstr "Installer en anden USB 2.0 PCIe-rodhub"

#. TRANSLATORS: the ColorHug is an open hardware product, don't translate the name
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:38
msgid ""
"MultiWriter was originally written as part of the ColorHug project but was "
"split off as an independent application in 2015."
msgstr ""
"Multiskriver blev oprindeligt skrevet som en del af ColorHug-projektet, men "
"blev et selvstændigt program i 2015."

#. TRANSLATORS: the 1st screenshot caption
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:47
msgid "Initial screen for the application"
msgstr "Programmets startskærm"

#. TRANSLATORS: the 2nd screenshot caption
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:52
msgid "Writing and verifying images"
msgstr "Skriver og verificerer aftryk"

#. TRANSLATORS: the 3rd screenshot caption
#: data/appdata/org.gnome.MultiWriter.appdata.xml.in:57
msgid "All devices have been written"
msgstr "Alle enheder er blevet skrevet"

#. TRANSLATORS: these are desktop keywords - remember the trailing ';' :)
#: data/org.gnome.MultiWriter.desktop.in:12
msgid "Image;Writer;ISO;USB;"
msgstr "Aftryk;Skriver;ISO;USB;"

#. TRANSLATORS: schema summary
#: data/org.gnome.MultiWriter.gschema.xml:6
msgid "The filename to write to the USB disks"
msgstr "Filnavnet som skal skrives til USB-diskene"

#. TRANSLATORS: schema description
#: data/org.gnome.MultiWriter.gschema.xml:8
msgid "The filename of the ISO image to write to the connected USB disks."
msgstr ""
"Filnavnet på ISO-aftrykket som skal skrives til de tilsluttede USB-diske."

#. TRANSLATORS: schema summary
#: data/org.gnome.MultiWriter.gschema.xml:13
msgid "The maximum number of threads to use"
msgstr "Maksimalt antal tråde som skal bruges"

#. TRANSLATORS: schema description
#: data/org.gnome.MultiWriter.gschema.xml:15
msgid "The maximum number of parallel threads to use when copying data."
msgstr ""
"Maksimalt antal parallelle tråde som skal bruges ved kopiering af data."

#. TRANSLATORS: schema summary
#: data/org.gnome.MultiWriter.gschema.xml:20
msgid "Completely blank drive when writing"
msgstr "Ryd drevet helt når der skrives"

#. TRANSLATORS: schema description
#: data/org.gnome.MultiWriter.gschema.xml:22
msgid "Write the ISO to the drive and then blank the remainder of the drive."
msgstr "Skriv ISO'en til drevet og ryd det resterende af drevet."

#. TRANSLATORS: schema summary
#: data/org.gnome.MultiWriter.gschema.xml:27
msgid "Verify the ISO image was written correctly"
msgstr "Verificer at ISO-aftrykket blev skrevet korrekt"

#. TRANSLATORS: schema description
#: data/org.gnome.MultiWriter.gschema.xml:29
msgid ""
"Read and verify the ISO image from each device after writing is complete."
msgstr ""
"Læs og verificer ISO-aftrykket fra hver enhed når det er blevet skrevet."

#. TRANSLATORS: schema summary
#: data/org.gnome.MultiWriter.gschema.xml:34
msgid "Inspect the device before writing images"
msgstr "Inspicer enheden inden der skrives aftryk"

#. TRANSLATORS: schema description
#: data/org.gnome.MultiWriter.gschema.xml:36
msgid ""
"Inspect the device to verify the reported device size is the actual media "
"size."
msgstr ""
"Inspicer enheden for at verificere at den rapporterede enhedsstørrelse er "
"den reelle mediestørrelse."

#. TRANSLATORS: schema summary
#: data/org.gnome.MultiWriter.gschema.xml:41
msgid "Show a warning before writing to a drive"
msgstr "Vis en advarsel inden der skrives til et drev"

#. TRANSLATORS: schema description
#: data/org.gnome.MultiWriter.gschema.xml:43
msgid ""
"Show a warning dialog confirming that the device contents are to be erased."
msgstr ""
"Vis en advarselsdialog til bekræftelse af, at enhedens indhold skal ryddes."

#: data/org.gnome.MultiWriter.policy.in:17
msgid "Check the device"
msgstr "Tjek enheden"

#: data/org.gnome.MultiWriter.policy.in:18
msgid "Authentication is required to probe the device"
msgstr "Der kræves autentifikation for at undersøge enheden"

#. TRANSLATORS: The image has been written and verified to
#. * *one* device, not all
#: src/gmw-device.c:211
msgid "Written successfully"
msgstr "Skrivning lykkedes"

#. TRANSLATORS: we're writing the image to the device
#. * and we now know the speed
#: src/gmw-device.c:219
#, c-format
msgid "Writing at %.1f MB/s…"
msgstr "Skriver med %.1f MB/s …"

#. TRANSLATORS: we're writing the image to the USB device
#: src/gmw-device.c:223
msgid "Writing…"
msgstr "Skriver …"

#. TRANSLATORS: We're verifying the USB device contains
#. * the correct image data and we now know the speed
#: src/gmw-device.c:232
#, c-format
msgid "Verifying at %.1f MB/s…"
msgstr "Verificerer med %.1f MB/s …"

#. TRANSLATORS: We're verifying the USB device contains
#. * the correct image data
#: src/gmw-device.c:237
msgid "Verifying…"
msgstr "Verificerer …"

#. TRANSLATORS: This is a generic no-name USB flash disk
#: src/gmw-device.c:294
msgid "USB Flash Drive"
msgstr "USB-flashdrev"

#. TRANSLATORS: window title renaming labels
#: src/gmw-main.c:263
msgid "New hub label"
msgstr "Ny etiket for hub"

#. TRANSLATORS: the application name
#. TRANSLATORS: the application name for the about UI
#. TRANSLATORS: A program to copy the LiveUSB image onto USB hardware
#: src/gmw-main.c:418 src/gmw-main.c:1243 src/gmw-main.c:1681
msgid "GNOME MultiWriter"
msgstr "GNOME Multiskriver"

#. TRANSLATORS: the success sound description
#: src/gmw-main.c:420
msgid "Image written successfully"
msgstr "Aftrykket blev skrevet"

#. TRANSLATORS: copy aborted
#: src/gmw-main.c:523 src/gmw-main.c:563 src/gmw-main.c:666 src/gmw-main.c:701
msgid "Cancelled"
msgstr "Annulleret"

#. TRANSLATORS: we couldn't open the ISO file the user chose
#: src/gmw-main.c:919 src/gmw-main.c:933
msgid "Failed to open"
msgstr "Kunne ikke åbne"

#. TRANSLATORS: window title for the file-chooser, file is an ISO
#: src/gmw-main.c:950
msgid "Choose the file to write"
msgstr "Vælg filen som skal skrives"

#. TRANSLATORS: button title
#. TRANSLATORS: button to cancel a copy process
#: src/gmw-main.c:954 src/gmw-main.ui:115
msgid "Cancel"
msgstr "Annuller"

#. TRANSLATORS: button title
#: src/gmw-main.c:956
msgid "Import"
msgstr "Importér"

#. TRANSLATORS: the file filter description, e.g. *.iso
#: src/gmw-main.c:960
msgid "ISO files"
msgstr "ISO-filer"

#. TRANSLATORS: error dialog title:
#. * we probably didn't authenticate
#: src/gmw-main.c:1104 src/gmw-main.c:1123
msgid "Failed to copy"
msgstr "Kunne ikke kopiere"

#. TRANSLATORS: the inhibit reason
#: src/gmw-main.c:1140
msgid "Writing ISO to devices"
msgstr "Skriver ISO til enheder"

#. TRANSLATORS: window title for the warning dialog
#: src/gmw-main.c:1187
msgid "Write to all disks?"
msgstr "Skriv til alle diske?"

#. TRANSLATORS: check that we can nuke everything from all disks
#: src/gmw-main.c:1192
msgid "All data on the drives will be deleted."
msgstr "Al data på drevene slettes."

#. TRANSLATORS: if the image file is smaller than the disks and
#. * we've disabled wiping the device we only write enough data
#. * to transfer the image
#: src/gmw-main.c:1197
msgid "The ISO file is smaller than the disk capacity."
msgstr "ISO-filen er mindre end diskkapaciteten."

#. TRANSLATORS: this could leave your personal files on the drive
#: src/gmw-main.c:1202
msgid ""
"Some of the current contents of the drives could be still found using "
"forensic tools even after copying."
msgstr ""
"Noget af det nuværende indhold på drevene kan stadig findes med tekniske "
"værktøjer, selv efter kopiering."

#. TRANSLATORS: button text for the warning dialog
#: src/gmw-main.c:1209
msgid "I Understand"
msgstr "Modtaget"

#. TRANSLATORS: the title of the about window
#: src/gmw-main.c:1241
msgid "About GNOME MultiWriter"
msgstr "Om GNOME Multiskriver"

#. TRANSLATORS: you can put your name here :)
#: src/gmw-main.c:1251
msgid "translator-credits"
msgstr "scootergrisen"

#. TRANSLATORS: a switch label: verify the image by
#. * reading back the original image from the device
#: src/gmw-main.c:1298
msgid "Verify"
msgstr "Verificer"

#. TRANSLATORS: a switch label: we write zeros after
#. * the image so it erases the entire device
#: src/gmw-main.c:1304
msgid "Wipe"
msgstr "Ryd"

#. TRANSLATORS: a switch label: we check the device
#. * is actually the size it says it is
#: src/gmw-main.c:1310
msgid "Probe"
msgstr "Undersøg"

#. TRANSLATORS: command line option
#: src/gmw-main.c:1665
msgid "Allow renaming the labels on hubs"
msgstr "Tillad omdøbning af etiketterne på hubber"

#. TRANSLATORS: command line option
#: src/gmw-main.c:1668 src/gmw-probe.c:468
msgid "Show extra debugging information"
msgstr "Vis ekstra fejlretningsinformation"

#. TRANSLATORS: the user has sausages for fingers
#: src/gmw-main.c:1686
msgid "Failed to parse command line options"
msgstr "Kunne ikke fortolke kommandolinjetilvalgene"

#. TRANSLATORS: button to start the copy
#: src/gmw-main.ui:102
msgid "Start Copying"
msgstr "Start kopiering"

#. TRANSLATORS: command line option
#: src/gmw-probe.c:471
msgid "Random seed for predictability"
msgstr "Basistal for tilfældig talgenerator"

#~ msgid "org.gnome.MultiWriter"
#~ msgstr "org.gnome.MultiWriter"
